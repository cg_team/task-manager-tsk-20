package ru.inshakov.tm.command;

import ru.inshakov.tm.api.service.IServiceLocator;

public abstract class AbstractCommand {

    protected IServiceLocator IServiceLocator;

    public AbstractCommand() {
    }

    public void setIServiceLocator(IServiceLocator IServiceLocator) {
        this.IServiceLocator = IServiceLocator;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute();

}
