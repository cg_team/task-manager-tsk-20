package ru.inshakov.tm.command.project;

import ru.inshakov.tm.command.AbstractProjectCommand;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.util.TerminalUtil;

public class ProjectCascadeRemoveCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-cascade-remove-by-id";
    }

    @Override
    public String description() {
        return "remove project and all project tasks by id";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT AND ALL TASKS CASCADE]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = IServiceLocator.getProjectTaskService().removeProjectById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
    }

}