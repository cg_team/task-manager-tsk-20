package ru.inshakov.tm.command.project;

import ru.inshakov.tm.command.AbstractProjectCommand;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "clear all projects";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT CLEAR]");
        IServiceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
    }

}
