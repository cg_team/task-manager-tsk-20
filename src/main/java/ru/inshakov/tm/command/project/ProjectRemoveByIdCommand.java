package ru.inshakov.tm.command.project;

import ru.inshakov.tm.command.AbstractProjectCommand;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "remove project by name";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = IServiceLocator.getProjectService().removeById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

}
