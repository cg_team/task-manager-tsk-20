package ru.inshakov.tm.command.task;

import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

public class TaskBindByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-bind-by-project-id";
    }

    @Override
    public String description() {
        return "task bind to project by project id";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = IServiceLocator.getProjectTaskService().bindTaskByProjectId(userId, projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }

}
