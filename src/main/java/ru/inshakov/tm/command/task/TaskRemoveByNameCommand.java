package ru.inshakov.tm.command.task;

import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String description() {
        return "remove task by name";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        IServiceLocator.getTaskService().removeOneByName(userId, name);
    }

}
