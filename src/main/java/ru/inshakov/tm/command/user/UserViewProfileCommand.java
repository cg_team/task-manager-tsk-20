package ru.inshakov.tm.command.user;

import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.model.User;

public class UserViewProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-view-profile";
    }

    @Override
    public String description() {
        return "view user profile";
    }

    @Override
    public void execute() {
        System.out.println("[VIEW PROFILE]");
        final User user = IServiceLocator.getAuthService().getUser();
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
    }

}

