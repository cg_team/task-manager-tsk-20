package ru.inshakov.tm.exception.empty;

public class EmptyStatusException extends RuntimeException {

    public EmptyStatusException() {
        super("Error! Status is empty...");
    }

}
