package ru.inshakov.tm.model;

import ru.inshakov.tm.api.entity.IWBS;
import ru.inshakov.tm.enumerated.Status;

import java.util.Date;

public class Project extends AbstractBusinessEntity implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date dateStart;

    private Date dateFinish;

    private Date created = new Date();

}
