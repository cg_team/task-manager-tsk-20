package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.IBusinessRepository;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public void clear(String userId) {

    }

    @Override
    public List<E> findAll(String userId) {
        return null;
    }

    @Override
    public List<E> findAll(
            final String userId, final Comparator<E> comparator
    ) {
        final List<E> entitiesSort = new ArrayList<>(entities);
        entitiesSort.sort(comparator);
        final List<E> allEntities = entitiesSort.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .collect(Collectors.toList());
        return allEntities;
    }

    @Override
    public E findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        for (final E entity: entities) {
            if (entity == null) continue;
            return entity;
        }
        return null;
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        final E entity = entities.get(index);
        if (userId.equals(entity.getUserId())) return entity;
        throw new AccessDeniedException();
    }

    @Override
    public E findOneByName(final String userId, final String name) {
        for (final E entity : entities) {
            if (name.equals(entity.getName()) &&
                    userId.equals(entity.getUserId())) return entity;
        }
        return null;
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        final E entity = findOneById(userId, id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        final E entity = findOneByName(userId, name);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

}
