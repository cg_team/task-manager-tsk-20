package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (final User user: entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user: entities) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        removeUser(user);
        return null;
    }

    private void removeUser(final User user) {
        entities.remove(user);
    }

}
