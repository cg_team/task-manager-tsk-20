package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.IBusinessRepository;
import ru.inshakov.tm.api.service.IBusinessService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.empty.EmptyStatusException;
import ru.inshakov.tm.exception.entity.EntityNotFoundException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    IBusinessRepository<E> entityRepository;

    public AbstractBusinessService(IBusinessRepository<E> entityRepository) {
        super(entityRepository);
        this.entityRepository = entityRepository;
    }

    @Override
    public E changeStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        final E entity = findOneById(userId, id);
        if (entity == null) throw new EntityNotFoundException();
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E changeStatusByName(final String userId, final String name, final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        final E entity = findOneByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public void clear(String userId) {
        entityRepository.clear(userId);
    }

    @Override
    public E finishById(final String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findOneById(userId, id);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E finishByName(final String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E finishByIndex(final String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (comparator == null) return null;
        return entityRepository.findAll(userId, comparator);
    }

    @Override
    public List<E> findAll(String userId) {
        return entityRepository.findAll(userId);
    }

    @Override
    public E findOneById(final String userId, String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return entityRepository.findOneById(userId, id);
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) {
            throw new IndexIncorrectException();
        }
        return entityRepository.findOneByIndex(userId, index);
    }

    @Override
    public E findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return entityRepository.findOneByName(userId, name);
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return entityRepository.removeOneById(userId, id);
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) {
            throw new IndexIncorrectException();
        }
        return entityRepository.removeOneByIndex(userId, index);
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return entityRepository.removeOneByName(userId, name);
    }

    @Override
    public E startById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findOneById(userId, id);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startByName(final String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        entity .setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneById(userId, id);
        if (entity == null) return null;
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}

